require 'invoicexpress/models'

module Invoicexpress
  class Client
    module Documents

      # Returns all your related documents
      #
      # @option options [Integer] page (1) You can ask a specific page of invoices
      #
      # @return [Array<Invoicexpress::Models::Invoice>] An array with all your related documents
      # @raise Invoicexpress::Unauthorized When the client is unauthorized
      def documents(invoice_id,options={})
        params = { :page => 1, :klass => Invoicexpress::Models::Invoices }

        get("document/#{invoice_id}/related_documents.xml", params.merge(options))
      end

    end

  end

end
